/*
 * Part of 2A flashlight driver, current stabilization.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "../../libs/stm8/system.h"
#include "modes.h"
#include "stabilizer.h"

extern volatile modeTyp mode;

void stabilizerInit()
{
    // feedback cheat init
    portConfig(FB_PORT, FB_PIN, INPUT_PULLUP);
    // adc init
    portConfig(ADC_PORT, AMP_PIN | VOLT_PIN, INPUT_FLOAT);
    ADC1_CR1  = SPSEL_DIV18 | CONT;
    ADC1_CR2  = SCAN;
    ADC1_CR3  = 0;
    ADC1_CSR  =  EOCIE | VOLT_CH_INIT;
    ADC1_TDRH = 0;
    ADC1_TDRL = 0;
    // maximal priority for fast stabilization
    setPriority(ADC1_ITN, LEVEL3);
    enable(ADC);
    ADC1_CR1 |= ADON;
    ADC1_CR1 |= ADON;
}

// interrupt on compliting the measurements
void feedback()
{
    static uint8_t current=0;
    current = (ADC1_DBRH(AMP_CH) + current) / 2;
//   switching between 3.9V and 2V using GPIO pullup
    if( current >= mode.current ) {
        P_CR1(FB_PORT) |= FB_PIN;
    } else {
        P_CR1(FB_PORT) &= ~FB_PIN;
    }
    ADC1_CSR  = EOCIE | VOLT_CH_INIT;
//   to avoid overrun
    for(uint8_t i=0 ; i<=VOLT_CH ; ++i) {
        (void)ADC1_DBRH(i);
        (void)ADC1_DBRL(i);
    }
    ADC1_CR3 = 0;
}
