/*
 * Part of 2A flashlight driver, choosing mode functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "../../libs/stm8/regs/tim_reg.h"
#include "../../libs/stm8/system.h"
#include "../../libs/stm8/eeprom.h"
#include "stabilizer.h"
#include "modes.h"

// current in amperes for every mode from enum
const uint8_t currentTable[MODES_NUM] = {   ((uint16_t)(0*SAMPLES_PER_AMP)>>2),
                                          ((uint16_t)(0.5*SAMPLES_PER_AMP)>>2),
                                          ((uint16_t)(1.9*SAMPLES_PER_AMP)>>2),
                                            ((uint16_t)(2*SAMPLES_PER_AMP)>>2) };
volatile modeTyp mode;

void strobeInit(void);

void selector()
{
    // reading mode parameter from eeprom and writing for the next switching on
    mode.mode = readByte(MODE_ADDR) + 1;
    if( mode.mode >= MODES_NUM ) {
        mode.mode = MODE_OFF;
    }
    if( unlockData() < 0 ) {
        mode.mode = MODE_OFF;
    } else {
        writeByte(MODE_ADDR, mode.mode);
    }
    mode.current = currentTable[mode.mode];
    // switching off
    portConfig(EN_PORT, EN_PIN, OUTPUT_PP_2M);
    if(mode.mode == MODE_OFF) {
        resetPin(EN_PORT, EN_PIN);
        return;
    }
    // switching on
    setPin(EN_PORT, EN_PIN);
    strobeInit();
    stabilizerInit();
}

void strobeInit()
{
   if(mode.mode != MODE_STROBE) {
       return;
   }
    enable(TIM1);
    TIM1_IER   = UIE;
    TIM1_CR1   = URS;
    TIM1_CR2   = 0;
    TIM1_SMCR  = 0;
    TIM1_ETR   = 0;
    TIM1_PSCRH = 255;
    TIM1_PSCRL = 255;
    TIM1_ARRH  = 0;
    TIM1_ARRL  = (uint8_t)STROBE_ARR;
    TIM1_SR1   = 0;
    TIM1_SR2   = 0;
    setPriority(TIM1_UPD_ITN, LEVEL1);
    TIM1_CR1  |= CEN;
    TIM1_EGR  = UG;
}

void stroboscope() {
    TIM1_SR1 = 0;
    TIM1_SR2 = 0;
    if( mode.current>0 ) {
        mode.current = 0;
    } else {
        mode.current = currentTable[mode.mode];
    }
}

