#ifndef H_STABILIZER
#define H_STABILIZER
/*
 * Part of 2A flashlight driver, current stabilization.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "../../libs/stm8/gpio.h"
#include "../../libs/stm8/regs/adc_reg.h"

// ports/channels (board depencies)
// feedback measure
#define ADC_PORT        GPIOD
#define VOLT_PIN        GPIO5
#define AMP_PIN         GPIO6
// pwm control outputs
#define EN_PORT         GPIOA
#define EN_PIN          GPIO1
#define FB_PORT         GPIOD
#define FB_PIN          GPIO4
// channels of ADC
#define AMP_CH_INIT     CH_AIN5
#define VOLT_CH_INIT    CH_AIN6
#define AMP_CH          5
#define VOLT_CH         6

// frequencies of feedback loop in HZ
#define CURRENT_HZ  800000

// voltages (multiplied by 10)
#define MEAN_VDD            3.7
#define VOUT_DEFAULT        3.9
#define VOUT_PULLUP         2.0
#define FB_TO_VOLT          7.8
#define FB_TO_VOLT_PULLUP   4
#define VOLTS_PER_AMPERE    0.5

// coefficients
#define MAX_ADC                 1024
#define SAMPLES_PER_VOLT        (uint16_t)((MAX_ADC/MEAN_VDD)/FB_TO_VOLT)
#define SAMPLES_PER_VOLT_PULLUP (uint16_t)((MAX_ADC/MEAN_VDD)/FB_TO_VOLT_PULLUP)
#define SAMPLES_PER_AMP         (uint16_t)((MAX_ADC/MEAN_VDD)*VOLTS_PER_AMPERE)

void stabilizerInit(void);
void feedback(void);

#endif
