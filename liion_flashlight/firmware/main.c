#include "../../libs/stm8/system.h"
#include "modes.h"
#include "stabilizer.h"


int main(void)
{
    clockTo16Hsi();
    selector();
    while(1) {
    }
}


void tim1_isr(void) __interrupt(TIM1_UPD_ITN)
{
    stroboscope();
}

void adc_isr(void) __interrupt(ADC1_ITN)
{
    feedback();
}
