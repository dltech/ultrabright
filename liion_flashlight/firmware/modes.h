#ifndef H_MODES
#define H_MODES
/*
 * Part of 2A flashlight driver, choosing mode functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "../../libs/stm8/system.h"
#include "stabilizer.h"

// mode definitions (recommended for tuning according to your preferences)
enum mode{
    MODE_OFF = 0,
    MODE_LOW,
    MODE_MAX,
    MODE_STROBE
};
#define MODES_NUM   MODE_STROBE+1

// frequency of stroboscopic mode
#define STROBE_HZ       15
#define STROBE_ARR      ((FSYSTEM/65536)/STROBE_HZ)/2

typedef struct {
    uint8_t current;
    uint8_t mode;
} modeTyp;

#define MODE_ADDR   0

void selector(void);
void stroboscope(void);

#endif
