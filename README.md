# Different lamps with microcontroller interfacing.

Devices list:

# 1. Cree XM-L2 flashlight driver, characteristics:\
*High-effiency buck-boost DC-DC.\
*Current feedback.\
*2A max current.\
*Programmable modes.\
*Compact size for building-in (17mm diameter).\
![](liion_flashlight/result/liion_flashlight_3d.png)
![](liion_flashlight/pcb/liion_flashlight_small.png)

# 2. Power supply for led light in the garden house, characteristics:\
*Current stabilization\
*28V 1A output\
*sub-ghz remote control\
*dimming\
*auto mode\
![](garden_house_light/pcb/sch_small.png)
![](garden_house_light/result/pcb_3d.png)

# 3. desk_lamp
4-ch pwm dimmer module with outputs for RGB and white LED strips.\
input: 60-240V AC\
output: 0-12V 2A per every channel, but 2A in summary.\
![](desk_lamp/pcb/pcb.png)
![](desk_lamp/pcb/pcb3d.png)

# 4. new year lights
Bluetooth configurable 11 led lights with beeper and microphone for color music. 5W power.
![](decorative_lights/pcb/pcb_small.png)
![](decorative_lights/pcb/pcb3db.png)
![](decorative_lights/pcb/pcb3d.png)

# 4. 5M RGB LED light controller, with meteostation and clock. Features:
*60W power supply\
*bluetooth controller\
*LCD1602 watch\
*meteostation with temperature and humiditry\
*3 2A voltage regulated outputs\
*PIR motion sensor 
![](kitchen_rgb_clock/clock.png)
![](kitchen_rgb_clock/power_supply_small.png)
![](kitchen_rgb_clock/pcb3db.png)
![](kitchen_rgb_clock/pcb3d.png)
