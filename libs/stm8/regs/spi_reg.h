#ifndef H_SPI_REG
#define H_SPI_REG
/*
 * STM8 serial peripherial interface (SPI) register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"


/* SPI control register 1 */
#define SPI_CR1     MMIO8(SPI_BASE + 0x00)
// Frame format
#define LSBFIRST    0x80
// SPI enable
#define SPE         0x40
// Baud rate control
#define BR_DIV2     0x00
#define BR_DIV4     0x08
#define BR_DIV8     0x10
#define BR_DIV16    0x18
#define BR_DIV32    0x20
#define BR_DIV64    0x28
#define BR_DIV128   0x30
#define BR_DIV256   0x38
// Master selection
#define MSTR        0x04
// Clock polarity
#define CPOL        0x02
// Clock phase
#define CPHA        0x01

/* SPI control register 2 */
#define SPI_CR2     MMIO8(SPI_BASE + 0x01)
// Bidirectional data mode enable
#define BDM     0x80
// Input/Output enable in bidirectional mode
#define BDOE    0x40
// Hardware CRC calculation enable
#define CRCEN   0x20
// Transmit CRC next
#define CRCNEXT 0x10
// Receive only
#define RXONLY  0x04
// Software slave management
#define SSM     0x02
// Internal slave select
#define SSI     0x01

/* SPI interrupt control register */
#define SPI_ICR     MMIO8(SPI_BASE + 0x02)
// Tx buffer empty interrupt enable
#define TXIE    0x80
// RX buffer not empty interrupt enable
#define RXIE    0x40
// Error interrupt enable
#define ERRIE   0x20
// Wakeup interrupt enable
#define WKIE    0x10

/* SPI status register */
#define SPI_SR      MMIO8(SPI_BASE + 0x03)
// Busy flag
#define BSY     0x80
// Overrun flag
#define OVR     0x40
// Mode fault
#define MODF    0x20
// CRC error flag
#define CRCERR  0x10
// Wakeup flag
#define WKUP    0x08
// Transmit buffer empty
#define TXE     0x02
// Receive buffer not empty
#define RXNE    0x01

/* SPI data register */
#define SPI_DR      MMIO8(SPI_BASE + 0x04)

/* SPI CRC polynomial register */
#define SPI_CRCPR   MMIO8(SPI_BASE + 0x05)

/* SPI Rx CRC register */
#define SPI_RXCRCR  MMIO8(SPI_BASE + 0x06)

/* SPI Tx CRC register */
#define SPI_TXCRCR  MMIO8(SPI_BASE + 0x07)

#endif
