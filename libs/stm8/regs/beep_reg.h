#ifndef H_BEEP_REG
#define H_BEEP_REG
/*
 * STM8 beeper register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"


/* Beeper control/status register */
#define BEEP_CSR    MMIO8(BEEP_BASE + 0x00)
// Beep selection
#define BEEPSEL8X   0x00
#define BEEPSEL4X   0x40
#define BEEPSEL2X   0x80
// Beep enable
#define BEEPEN      0x20
// Beep prescaler divider
#define BEEPDIV2    0x00
#define BEEPDIV3    0x01
#define BEEPDIV4    0x02
#define BEEPDIV5    0x03
#define BEEPDIV6    0x04
#define BEEPDIV7    0x05
#define BEEPDIV8    0x06
#define BEEPDIV9    0x07
#define BEEPDIV10   0x08
#define BEEPDIV11   0x09
#define BEEPDIV12   0x0a
#define BEEPDIV13   0x0b
#define BEEPDIV14   0x0c
#define BEEPDIV15   0x0d
#define BEEPDIV16   0x0e
#define BEEPDIV17   0x0f
#define BEEPDIV18   0x10
#define BEEPDIV19   0x11
#define BEEPDIV20   0x12
#define BEEPDIV21   0x13
#define BEEPDIV22   0x14
#define BEEPDIV23   0x15
#define BEEPDIV24   0x16
#define BEEPDIV25   0x17
#define BEEPDIV26   0x18
#define BEEPDIV27   0x19
#define BEEPDIV28   0x1a
#define BEEPDIV29   0x1b
#define BEEPDIV30   0x1c
#define BEEPDIV31   0x1d
#define BEEPDIV32   0x1e
#define BEEPDIV(n)  (n-2)


#endif
