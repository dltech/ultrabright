#ifndef H_UART_REG
#define H_UART_REG
/*
 * STM8 uart register definitions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "memorymap.h"
#include "../system.h"

/* Status register */
#define UART1_SR    MMIO8(UART1_BASE + 0x00)
// Transmit data register empty
#define TXE     0x80
// Transmission complete
#define TC      0x40
// Read data register not empty
#define RXNE    0x20
// IDLE line detected
#define IDLE    0x10
// LIN Header Error (LIN slave mode)
// Overrun error
#define OR      0x08
#define LHE     0x08
// Noise flag
#define NF      0x04
// Framing error
#define FE      0x02
// Parity error
#define PE      0x01

/* Data register */
#define UART1_DR    MMIO8(UART1_BASE + 0x01)
// Contains the Received or Transmitted data character.

/* Baud rate register 1 */
#define UART1_BRR1  MMIO8(UART1_BASE + 0x02)
// UART_DIV[11:4]

/* Baud rate register 2 */
#define UART1_BRR2  MMIO8(UART1_BASE + 0x03)
// UART_DIV[15:12]
// UART_DIV[3:0]
#define CALC_BRR1(baud) ((uint8_t)(((FSYSTEM/baud)>>4)&0xff))
#define CALC_BRR2(baud) ((uint8_t)(((((FSYSTEM/baud)>>12)&0x0f)<<4) +\
                                     ((FSYSTEM/baud)&0x0f) ))

/* Control register 1 */
#define UART1_CR1   MMIO8(UART1_BASE + 0x04)
// Receive Data bit 8.
#define R8      0x80
// Transmit data bit 8.
#define T8      0x40
// UART Disable (for low power consumption).
#define UARTD   0x20
// word length.
#define M       0x10
// Wakeup method
#define WAKE    0x08
// Parity control enable.
#define PCEN    0x04
// Parity selection.
#define PS      0x02
// Parity interrupt enable.
#define PIEN    0x01

/* Control register 2 */
#define UART1_CR2   MMIO8(UART1_BASE + 0x05)
// Transmitter interrupt enable.
#define TIEN    0x80
// Transmission complete interrupt enable.
#define TCIEN   0x40
// Receiver interrupt enable.
#define RIEN    0x20
// IDLE Line interrupt enable.
#define ILIEN   0x10
// Transmitter enable
#define TEN     0x08
// Receiver enable
#define REN     0x04
// Receiver wakeup
#define RWU     0x02
// Send break
#define SBK     0x01

/* Control register 3 */
#define UART1_CR3   MMIO8(UART1_BASE + 0x06)
// LIN mode enable.
#define LINEN       0x40
// STOP bits.
#define STOP_1      0x00
#define STOP_2      0x02
#define STOP_1P5    0x03
// Clock enable.
#define CLKEN       0x08
// Clock polarity.
#define CPOL        0x04
// Clock phase.
#define CPHA        0x02
// Last bit clock pulse.
#define LBCL        0x01

/* Control register 4 */
#define UART1_CR4   MMIO8(UART1_BASE + 0x07)
// LIN Break Detection Interrupt Enable.
#define LBDIEN  0x40
// LIN Break Detection Length.
#define LBDL    0x20
// LIN Break Detection Flag.
#define LBDF    0x10
// Address of the UART node.
#define LADD_MSK    0x0f

/* Control register 5 */
#define UART1_CR5   MMIO8(UART1_BASE + 0x08)
// Smartcard mode enable.
#define SCEN    0x20
// Smartcard NACK enable.
#define NACK    0x10
// Half-Duplex Selection.
#define HDSEL   0x08
// IrDA Low Power.
#define IRLP    0x04
// IrDA mode Enable.
#define IREN    0x02

// /* Control register 6 */
// #define UART2_CR6   MMIO8(UART2_BASE + 0x09)
// // LIN Divider Update Method.
// #define LDUM    0x80
// // LIN Slave Enable.
// #define LSLV    0x20
// // LIN automatic resynchronisation enable.
// #define LASE    0x10
// // LIN Header Detection Interrupt Enable.
// #define LHDIEN  0x04
// // LIN Header Detection Flag.
// #define LDHF    0x02
// // LIN Sync Field.
// #define LSF     0x01

/* Guard time register */
#define UART1_GTR   MMIO8(UART1_BASE + 0x09)
// Guard time value in terms of number of baud clocks.

/* Prescaler register */
#define UART1_PSCR  MMIO8(UART1_BASE + 0x0a)
// prescaler for system clock to achieve the low power frequency
#define IRDA_PSC_MSK        0xff
// prescaler for system clock to provide the smartcard clock.
#define SMARTCARD_PSC_MSK   0xf1f



#endif
