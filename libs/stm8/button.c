/*
 * STM8 basic support library. STM8 interrupt based button callbacks.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "regs/interrupt_reg.h"
#include "gpio.h"
#include "button.h"

void butonInit(uint8_t port, uint8_t pin)
{
    portConfig(BUTTON_PORT, BUTTON_PIN, INPUT_PULLUP_EXTI);
    switch(BUTTON_PORT)
    {
        case PORTA:
            EXTI_CR1 &= ~PAIS_MASK;
            EXTI_CR1 = PAIS_FALL_LOW;
            return;
        case PORTB:
            EXTI_CR1 &= ~PBIS_MASK;
            EXTI_CR1 = PBIS_FALL_LOW;
            return;
        case PORTC:
            EXTI_CR1 &= ~PCIS_MASK;
            EXTI_CR1 = PCIS_FALL_LOW;
            return;
        case PORTD:
            EXTI_CR1 &= ~PDIS_MASK;
            EXTI_CR1 = PDIS_FALL_LOW;
            return;
        case PORTE:
            EXTI_CR2 &= ~PEIS_MASK;
            EXTI_CR2 = PEIS_FALL_LOW;
            return;
    }
    setPriority(EXTI4_ITN, 0);
}


