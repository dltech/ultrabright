#ifndef H_MODES
#define H_MODES
/*
 * Part of garden light smart power supply, choosing mode functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "lib/system.h"
#include "lib/gpio.h"

// button ports
#define BUTTON_PORT1    GPIOA
#define BUTTON_UP       GPIO3
#define BUTTON_PORT2    GPIOB
#define BUTTON_DN       GPIO4
#define BUTTON_AUTO     GPIO5
// led ports
#define LED_PORT        GPIOC
#define LED_ON          GPIO7
#define LED_MAX         GPIO6
#define LED_AUTO        GPIO3

// mode config
#define MIN_CURRENT     0.1
#define MAX_CURRENT     0.8
#define NSTEP           8

// mode definitions (recommended for tuning according to your preferences)
enum mode{
    MODE_OFF = 0,
    MODE_MANUAL,
    MODE_AUTO_SUN,
    MODE_AUTO_IR
};
#define MODES_NUM       MODE_AUTO_IR+1

typedef struct {
    uint8_t current;
    uint8_t mode;
} modeTyp;

#define MODE_ADDR       0
#define CURRENT_ADDR    1

void uiInit(void);
void buttonCallback(void);
void sensorHandler(void);

#endif
