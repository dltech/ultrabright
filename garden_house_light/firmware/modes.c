/*
 * Part of garden light smart power supply, choosing mode functions.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "lib/regs/tim_reg.h"
#include "lib/system.h"
#include "lib/eeprom.h"
#include "lib/button.h"
#include "modes.h"

// current in amperes for every mode from enum
volatile modeTyp mode;

void autoInit(void)


void uiInit()
{
    // gpio init
    butonInit(BUTTON_PORT1, BUTTON_UP);
    butonInit(BUTTON_PORT2, BUTTON_DN | BUTTON_AUTO);
    portConfig(LED_PORT, LED_ON | LED_MAX | LED_AUTO, OUTPUT_PP_2M);
    resetPin(LED_PORT, LED_ON | LED_MAX | LED_AUTO);
    // restore previous settings
    mode.mode = readByte(MODE_ADDR);
    mode.current = readByte(CURRENT_ADDR);
    autoInit();
}

void autoInit()
{
;
}

void buttonCallback()
{
;
}

void sensorHandler()
{
;
}
