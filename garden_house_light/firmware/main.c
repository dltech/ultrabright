#include "lib/system.h"
#include "modes.h"
#include "stabilizer.h"


int main(void)
{
    clockTo16Hsi();
    uinInit();
    stabilizerInit();
    while(1) {
    }
}


void tim1_isr(void) __interrupt(TIM1_UPD_ITN)
{
    sensorHandler();
}

void adc_isr(void) __interrupt(ADC1_ITN)
{
    feedback();
}
