#ifndef H_STABILIZER
#define H_STABILIZER
/*
 * Part of garden light smart power supply, current stabilization.
 *
 * Copyright 2023 Mikhail Belkin <dltech174@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "lib/gpio.h"
#include "lib/regs/adc_reg.h"

// ports/channels (board depencies)
// feedback measure
#define ADC_PORT        GPIOD
#define AMP_PIN         GPIO5
#define LIGHT_PIN       GPIO3
#define IR_PIN          GPIO2
#define VOLT_PIN        GPIO6
// pwm control outputs
#define EN_PORT         GPIOA
#define FB_PIN          GPIO1
// channels of ADC
#define AMP_CH          CH_AIN5
#define LIGHT_CH        CH_AIN4
#define IR_CH           CH_AIN3
#define VOLT_CH         CH_AIN6

// frequencies of feedback loop in HZ
#define CURRENT_HZ  800000

// voltages (multiplied by 10)
#define MEAN_VDD            5
#define VOUT_DEFAULT        30.5
#define VOUT_PULLUP         10.2
#define FB_TO_VOLT          13.2
#define FB_TO_VOLT_PULLUP   5
#define VOLTS_PER_AMPERE    1.1

// coefficients
#define MAX_ADC                 1024
#define SAMPLES_PER_VOLT        (uint16_t)((MAX_ADC/MEAN_VDD)/FB_TO_VOLT)
#define SAMPLES_PER_VOLT_PULLUP (uint16_t)((MAX_ADC/MEAN_VDD)/FB_TO_VOLT_PULLUP)
#define SAMPLES_PER_AMP         (uint16_t)((MAX_ADC/MEAN_VDD)*VOLTS_PER_AMPERE)

void stabilizerInit(void);
void feedback(void);

#endif
